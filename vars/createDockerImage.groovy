def call ( String jdkVersion = 'jdk-16.0.2' ) {
	DockerStr = libraryResource ( 'templates/Dockerfile' )

	def appVersion = sh script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true
	def artId  = sh script: 'mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout', returnStdout: true
	def variables = [ 
		'_version_' : appVersion,
		'_art_id_' : artId,
		// '_javaops_' :'' ,			
		'_memory_' : env.JAVA_MEMORY ,
		'_def_image_' : env.DEF_IMAGE						
	]
					
	// Reading variable from properties file if exists
	if ( fileExists ( 'variables.properties' ) ) {
		variables = readProperties defaults: variables, interpolate: true, file: 'variables.properties'

	}
	
	// if ( jdkVersion == 'JDK 11u2' ) {
	//     variables._def_image_ = 'hrlharbor-dev.harel-office.com/devops/base_jre11_integ:latest'
	// }

	
	DockerStr = global.renderTemplate ( DockerStr, variables )

	println "file with replaced values ${DockerStr}"
	writeFile file: 'Dockerfile', text: DockerStr

	try{
		sh """
		 docker build -t "${NEW_IMAGE_NAME}:${env.BUILD_NUMBER}" -t "${NEW_IMAGE_NAME}:latest" .
		"""
	}
	catch(all){
		global.DockerImageBuildFailure()
		sh "exit 1"
	}
}