def call () {

// 	if ( fileExists ( 'variables.properties' ) ) {
// 		variables = readProperties defaults: variables, interpolate: true, file: 'variables.properties'
// 	}

    try {
        withDockerRegistry(credentialsId: 'docker') {
        	sh '''docker pull "${NEW_IMAGE_NAME}:latest"
        	docker tag "${NEW_IMAGE_NAM}:latest" "${NEW_IMAGE_NAME}:${BUILD_NUMBER}"
        	docker tag "${NEW_IMAGE_NAM}:latest" "${NEW_IMAGE_NAME}:latest"
        	'''
        }
    } catch ( e ) {
		global.DockerPullTagFailure ()
	}
}