def call () {
	deployManifestTemp =  libraryResource ( env.MANIFEST_TEMPLATE )
	def variables = [
		'_proj_name_'  : env.PROJECT_NAME,
		'_image_name_' : "${PUSH_REPO}/${PROJECT_NAME}",
		'_replicas_'  : env.REPLICAS,
		'_service_type_'  : env.SERVICE_TYPE,
		'_namespace_' :  "default" // default  
	
	]
	
	// Getting manifest file from project repo if exists
	if ( fileExists ( env.MANIFEST_NAME ) ) {
		deployManifestTemp = readFile MANIFEST_NAME
	}
	
	// Reading variable from properties file if exists
	if ( fileExists ( 'variables.properties' ) ) {
		variables = readProperties defaults: variables, 
		interpolate: true, 
		file: 'variables.properties'
	}

	deployManifestTemp =  global.renderTemplate ( deployManifestTemp, variables )
	println """Template manifest \r\n$deployManifestTemp"""
	writeFile file: 'manifestGenerated.yml', text: deployManifestTemp

	// Reading volumes.yml from project repo if exists
	if ( fileExists ( 'volumes.yml' ) ) {
		updateVolumes ( 
			volumes_file:'volumes.yml',
			manifest_file: 'manifestGenerated.yml'
		)
	}
	
	// Read services template from library, render and importing into manifest
    def serviceTemp = libraryResource ( 'templates/dev/service.yml' )
    serviceTemp = global.renderTemplate ( serviceTemp, variables )
    // sh 'rm -rfv service.yml'
    writeFile file: 'service.yml', text: serviceTemp
    sh '''
        echo 'SERVICE.yml'
        cat service.yml
    '''

	withKubeConfig ( [ credentialsId: "${SERVICE_ACCOUNT}" ] ) {
		sh script: '''
			kubectl delete -f manifestGenerated.yml --ignore-not-found
			kubectl apply -f manifestGenerated.yml --record=true
			kubectl delete -f service.yml --ignore-not-found
			kubectl apply -f service.yml --record=true
		'''
	}        
}