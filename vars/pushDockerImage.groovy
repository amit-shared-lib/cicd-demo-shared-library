def call () {
// 	// Reading variable from properties file if exists
// 	if ( fileExists ( 'variables.properties' ) ) {
// 		variables = readProperties defaults: variables, interpolate: true, file: 'variables.properties'
// 	}
	
    // echo "${variables}"
	

	try {
		withDockerRegistry(credentialsId: 'docker') {
			sh '''#docker push "${NEW_IMAGE_NAME}:${BUILD_NUMBER}"
			docker push "${NEW_IMAGE_NAME}:latest"
			'''
		}
	}
	catch ( all ) {
		global.DockerPushFailure ()
	}
}