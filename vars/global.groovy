	import groovy.text.StreamingTemplateEngine

class global {
// Global variable and functions

	def void StartBuildMessage () {
		println '''
	###########################################################
	#               Starting a pipeline script                #
	#                    For CI procedure                     #
	###########################################################
	   '''
	}

	def void MavenBuildFailure () {
		println '''
	###########################################################
	#                 MAVEN BUILD FAILED                      #
	#             you might check your pom file               #
	#          or if you named the command correctly          #
	###########################################################
	error ( 'MAVEN BUILD FAILED' )
	   '''
	}

	def void DockerImageBuildFailure () {
		println '''
	###########################################################
	#                   BUILD IMAGE FAILED                    #
	#             you might check if the dockerfile           #
	#              is corrupted, or if you named              #
	#                  the command correctly                  #
	###########################################################
	error ( 'BUILD IMAGE FAILED' )
	        '''
	}

	def void DockerLoginFailure () {
		println '''
	###########################################################
	#                    LOGIN DTR FAILED                     #
	#              you might check the dtr hosname            #
	#              or your credentials for this login         #
	###########################################################
	error ( 'LOGIN DTR FAILED' )
		'''
	}

	def void DockerPushFailure () {
		println '''
	############################################################
	#                    DOCKER PUSH FAILED                    #
	#            you might check if you named the              #
	#           repository correctly, or if you have           #
	#             permission to that repository	               #
	############################################################
	error ( 'DOCKER PUSH FAILED' )
	    '''
	}

	def void DockerPullTagFailure () {
		println '''
	############################################################
	#               DOCKER PULL AND TAG FAILED                 #
	#            you might check if you named the              #
	#           repository correctly, or if you have           #
	#             permission to that repository	               #
	############################################################
	error ( 'DOCKER PULL TAG FAILED' )
	    '''
	}

	def String renderTemplate ( String input, variables ) {
    	def engine = new StreamingTemplateEngine ()
    	return engine.createTemplate ( input ).make ( variables ).toString()
	}
}

