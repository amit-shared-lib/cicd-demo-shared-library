def call (){
	try {
        withMaven(maven: 'Apache Maven') {
            sh """
            echo JAVA_HOME::: ${JAVA_HOME}
            mvn clean install
            """
        }
	} catch ( err ) {
		global.MavenBuildFailure ()
	}
// 	timeout ( time: 1, unit: 'HOURS' ) { // Just in case something goes wrong, pipeline will be killed after a timeout
//         sonarResult = waitForQualityGate abortPipeline: false
// 		println sonarResult
//     }
}