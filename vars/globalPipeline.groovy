def call ( String jdkVersion = 'jdk-16.0.2' ) {
	global.StartBuildMessage ()

    pipeline {

        agent { label 'osboxes' }
        
        tools {
            maven 'Apache Maven'
            jdk jdkVersion
        }
        
        // options {
        //     ansiColor ( 'xterm' )
        // }

        stages {
            stage ( 'Set Environment Variables' ) {
                steps {
                    script {
                        println ">>>>>>>>> GIT_URL: ${GIT_URL}"
                        checkout scm
                        setEnvVariables ()
                    }
                }
            }

            stage ( 'Build' ) {
                when { 
                    not {
                        anyOf{ 
                            environment name: 'BRANCH_NAME', value: 'test'
                            environment name: 'BRANCH_NAME', value: 'main'
                        }
                    }
                }
                steps {
                    script {
                        mvnBuild ()
                    }
                }
            }

            stage ( 'Create image' ) {
                when { 
                    not {
                        anyOf{ 
                            environment name: 'BRANCH_NAME', value: 'test'
                            environment name: 'BRANCH_NAME', value: 'main'
                        }
                    }
                }
                steps {
                    script {
                        createDockerImage ( jdkVersion )
                    }
                }
            }


            stage ( 'Push image to Harbor' ) {
                when { 
                    not {
                        expression {BRANCH_NAME == 'test'}

                    }
                }
                steps {
                    script {
                        pushDockerImage ()
                    }
                }
            }

            stage ( 'Pull image from harbor and Tag' ) {
                steps {
                    script {
                        pullTagDockerImage ()
                    }
                }
            }
          
            stage ( 'Deploy to k8s' ) {
                steps {
                    script {
                    println '\033[1;34;7mConverting ' +  'Deploy to k8s :: ' + '\033[0m' 
                    DeployTok8s ()
                    }
                }
            }
        }
    
        post {
            always {
                emailNotify()
            }

            success {
                cleanWs()
            }
        }
    }
} 