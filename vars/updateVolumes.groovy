def call ( def parameters ) {
    def volumes_file = parameters.volumes_file
    def manifest_file = parameters.manifest_file
    def manifest = readYaml file: manifest_file
    def volumes = readYaml file: volumes_file
    
    try {
        volumes.volumes.eachWithIndex { volumesData, index ->
            if ( index == 0 ) {
                manifest.spec.template.spec.volumes = []
                manifest.spec.template.spec.containers.first().volumeMounts = []
            }
            manifest.spec.template.spec.containers.first().volumeMounts.add ( [:] )
            manifest.spec.template.spec.containers.first().volumeMounts[index].put ( 'name', volumesData.name )
            manifest.spec.template.spec.containers.first().volumeMounts[index].put ( 'mountPath', volumesData.mountPath )

            volumesData.remove ( 'mountPath' )
            manifest.spec.template.spec.volumes.add ( volumesData )
        }
    }
    catch ( err ) {
        println err.getMessage()
        error ( 'Info: Inject Volumes Into Manifest Template Failed!!!' )
    }
    finally {
        // Rewrite manifest file
        sh """
            echo "Removing  ${manifest_file} :::::"
            rm -f ${manifest_file}
         """
        writeYaml file: manifest_file, data: manifest
        sh """
            echo "Manifest ${manifest_file} with volumes "
            cat ${manifest_file}
         """
    }
}