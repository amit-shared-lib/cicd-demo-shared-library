def call () {

    def logContent = Jenkins.getInstance().getItemByFullName( env.JOB_NAME ).getBuildByNumber ( Integer.parseInt ( env.BUILD_NUMBER ) ) .logFile.text
    def logList = logContent.readLines()
    logList.each {
        if ( it =~ /you can browse/ ) {
            println 'FOUND Sonar project link ::: ' + it
            env.sonarReportLink = it.split ( 'you can browse' )[1].trim()
            return;
        }
    }
    
    def variables = [
		'name_of_build_runner'  : currentBuild.getBuildCauses().userName[0].toString(),
		'HAREL_CATEGORY_NAME' : 'Integration',
		'sonarReportLink' :  env.sonarReportLink, // default
		'job_name' : "<a href='${env.JOB_URL}'> '${JOB_BASE_NAME}' </a>",
		'build_number' : env.BUILD_NUMBER,
		'blue_ocen_link': "<a href='${env.JOB_URL}${env.BUILD_NUMBER}/console'> '${JOB_BASE_NAME}' </a>"
	]
    if ( currentBuild.result == 'SUCCESS' ) {
        env.emailTmp = libraryResource ( 'templates/success_email_template.html' )
    } else {
        env.emailTmp = libraryResource ( 'templates/failed_email_template.html' )
    }
    env.emailTmp = global.renderTemplate ( env.emailTmp, variables )



    emailSubject = "Build ${currentBuild.currentResult} for ${env.JOB_NAME} "
    emailBody = """
    $emailTmp
    """
    
    emailext attachLog: false,
    compressLog: false ,
    subject: emailSubject,
    body: emailBody,
    recipientProviders: [
        requestor(),
        developers(),
        brokenBuildSuspects(),
        brokenTestsSuspects(),
        upstreamDevelopers(),
        culprits()
    ]
}