def call () {
    try {     
        println "JOB_NAME in setEnvVariables ::: ${env.JOB_NAME}"
        env.PATH         = "${env.JAVA_HOME}/bin:${env.PATH}"

        switch ( BRANCH_NAME ) {
            case 'main':
            env.CLUSTER_NAME      = 'prod'
            env.SERVICE_ACCOUNT   = 'amit-svc'
            env.REPLICAS          = 3
            env.branchName        = 'main' 
            break;

            case 'test':
            env.CLUSTER_NAME      = 'test'
            env.SERVICE_ACCOUNT   = 'amit-svc'
            env.REPLICAS          = 1
            env.branchName        = 'test'
            break;

            default:
            env.CLUSTER_NAME      = 'dev'
            env.SERVICE_ACCOUNT   = 'amit-svc'
            env.REPLICAS          = 1
            env.branchName        = 'dev'
        }

        env.PUSH_REPO            = 'amitshemeshcrowd'
        env.PROJECT_NAME         = env.JOB_NAME.split('/')[-2].split('%2F')[-1]
        env.NEW_IMAGE_NAME       = "amitshemeshcrowd/springboot-app"
        env.SERVICE_NAME         = PROJECT_NAME
        env.MANIFEST_NAME        = 'manifest.yml'
        env.MANIFEST_TEMPLATE    = 'templates/dev/manifest.yml'
        env.VOLUMES_TEMPLATE     = 'volumes.yml'
        env.DEF_IMAGE            = "java"
        env.SERVICE_TYPE         = 'NodePort'
        println 'Deploy settings were set'
    }	catch ( err ) {
        error ( err.getMessage() );
    }
}
